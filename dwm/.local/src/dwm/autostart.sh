#! /bin/sh
#xrandr --output DisplayPort-0 --primary --mode 2560x1440 --pos 0x1080 --rotate normal --output DisplayPort-1 --off --output HDMI-A-0 --mode 1920x1080 --pos 2560x952 --rotate right --output DVI-I-0 --mode 1920x1080 --pos 320x0 --rotate normal
nitrogen --restore &
wal -R
sxhkd &

while true; do
  xsetroot -name "$(date)"
  sleep 1m # update displayed time every minute
done &
