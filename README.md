# papojari's config

## ToC

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [papojari's config](#papojaris-config)
	- [ToC](#toc)
	- [Preview images](#preview-images)
		- [bspwm](#bspwm)
			- [Google color scheme](#google-color-scheme)
			- [Dracula color scheme](#dracula-color-scheme)
	- [Wayland or Xorg?](#wayland-or-xorg)
	- [Dependencies](#dependencies)
		- [Arch Linux](#arch-linux)
			- [Wayland specific](#wayland-specific)
			- [Xorg specific](#xorg-specific)
		- [Other Unix'](#other-unix)
	- [Installation](#installation)
		- [Setting zsh as the shell](#setting-zsh-as-the-shell)
		- [linking shell scripts to `dash` instead of `bash`](#linking-shell-scripts-to-dash-instead-of-bash)
		- [Powerlevel10k prompt](#powerlevel10k-prompt)
		- [Backgrounds](#backgrounds)
			- [Xorg](#xorg)
		- [Xorg](#xorg)
		- [Wayland](#wayland)
		- [Pipewire](#pipewire)
	- [Setting one of the themes](#setting-one-of-the-themes)
		- [pywal](#pywal)
			- [Dracula](#dracula)
			- [Google](#google)
		- [lxappearance](#lxappearance)
		- [spicetify](#spicetify)
	- [Shortcuts](#shortcuts)
		- [sway (Wayland)](#sway-wayland)
		- [sxhkd (Xorg)](#sxhkd-xorg)
	- [Credits](#credits)
	- [Licenses](#licenses)
		- [alacritty license:](#alacritty-license)
		- [bspwm and sxhkd license:](#bspwm-and-sxhkd-license)
		- [spicetify community themes (which occur in modified form in the "Pywal" theme) license:](#spicetify-community-themes-which-occur-in-modified-form-in-the-pywal-theme-license)
		- [picom license:](#picom-license)
		- [Dracula Theme license:](#dracula-theme-license)
		- [Rate Arch Mirrors license](#rate-arch-mirrors-license)
		- [dwm license](#dwm-license)
		- [sway license](#sway-license)

<!-- /TOC -->

## Preview images

### bspwm

#### Google color scheme

![preview Google color scheme desktop](preview-google.png)

#### Dracula color scheme

![preview Dracula color scheme desktop](preview-dracula.png)

## Wayland or Xorg?

Before continuing decide if you want to use **Wayland** or **Xorg** or both as a display server (protocol). Xorg is still widely used but is being slowly replaced by Wayland. The Xorg developers decided bothering with Xorg wasn't worth it anymore and they needed something better. With Xorg for example you can have a window manager, a hotkey daemon and a wallpaper setter while with Wayland the compositor, which in my case is **sway** does all of that and is way less complicated to configure. There are many more reasons why Wayland is better. You could argue that Wayland has a wider support of programs because it can run both Wayland and Xorg programs. So when theres a heading for both Wayland and Xorg you only need to do one.

## Dependencies

Of course be free to change out some the software or leave it out like the terminal or web browser, that are not critical to the rest of the programs. This is just exactly the programs I use.

### Arch Linux

Package names for `pacman` and the AUR:

    alacritty spicetify-cli python-pywal chromeos-gtk-theme-git materia-gtk-theme breeze-snow-cursor-theme papirus-icon-theme stow spotify dash zsh zsh-syntax-highlighting autojump zsh-autosuggestions xrdb pipewire pipewire-pulse pipewire-alsa pactl pavucontrol bitwarden-bin ferdi-bin lxappearance nerd-fonts-ubuntu-mono ttf-font-awesome atom nemo heroic-games-launcher-bin steam inkscape blender godot gimp wget neofetch cava audacity brave-beta-bin ttc-iosevka nitrogen

#### Wayland specific

[Wayland](https://wiki.archlinux.org/title/Wayland) is the display server protocol in success to Xorg. If everything you need is supported on Wayland, go with Wayland.

    sway wofi slurp grim waybar xorg-xwayland

#### Xorg specific

    xorg-server bspwm picom sxhkd polybar

### Other Unix'

Please look in your package manager for these packages or look in the programs source code repository for releases/binaries or on how to compile that program.

## Installation

### Setting zsh as the shell

    chsh -s /usr/bin/zsh

### linking shell scripts to `dash` instead of `bash`

This command will redirect shell scripts, that use the `/bin/sh` link to `/bin/dash`

    ln -sfT dash /usr/bin/sh

### Powerlevel10k prompt

    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k

### Backgrounds

- [Dracula Dark Wallpaper Collection](https://draculatheme.com/wallpaper)

		cd /usr/share/backgrounds && sudo wget https://github.com/dracula/wallpaper/archive/master.zip && sudo ex master.zip

- [Google bubbles background](https://longwallpapers.com/Desktop-Wallpaper/Google%20Wallpaper.png)

		cd /usr/share/backgrounds && sudo mkdir Google && cd Google && sudo wget https://longwallpapers.com/Desktop-Wallpaper/Google%20Wallpaper.png

I edited the wallpaper to not include the Google and TTT watermarks. I don't want to distribute the wallpaper in this repository because I don't know the license nor the original author, whose are sadly nowhere to be found on the website nor on [tineye Reverse Image Search](https://longwallpapers.com/Desktop-Wallpaper/Google%20Wallpaper.png) for example.

- [Arch Linux chan](https://i.redd.it/09ds0jldh3x61.png)

		cd /usr/share/backgrounds && mkdir Arch && cd Arch && wget https://codeberg.org/papojari/config/raw/branch/master/wallpapers/ArchLinuxChan.png

  - Credits to [raviolimavioli](https://www.instagram.com/raviolimavioli/) for the background

#### Xorg

    nitrogen

to configure your backgrounds. If you followed the instruction above the wallpapers should be in `/usr/share/backgrounds`.

### Xorg

To use start either **bspwm** or **dwm** you obviously need to setup [**Xorg**](https://wiki.archlinux.org/index.php/Xorg). Configure your monitors too so you won't have to do it every boot with `xrandr`

If you decide to install [dwm](https://dwm.suckless.org/)

> dynamic window manager for X

do

    cd ~/config/dwm/.local/src/dwm/ && sudo make clean install

to compile my dwm configuration.

If you decide to use [bspwm](https://github.com/baskerville/bspwm), make sure to adjust ~/config/bspwm/.config/bspwm/bspwmrc and ~/config/polybar/.config/polybar/colorblocks/config.ini to your monitor configuration. You can find out the names of you monitors with `xrandr`.

### Wayland

Before you start **sway** adjust `~/config/sway/.config/sway/config` to you monitor setup. You can just start **sway** with `sway` in a tty.

### Pipewire

To have audio and my volume keybinds work you need to enable pipewire and start it if you want to use it without a reboot.

    systemctl --user enable pipewire pipewire-pulse && systemctl --user start pipewire pipewire-pulse

## Setting one of the themes

I've my onfiguration is laid out for 2 color schemes. These are **[Dracula](https://draculatheme.com/)** and **Google**.

### pywal

#### Dracula

    wal --theme base16-dracula

#### Google

    wal --theme base16-google

### lxappearance

Start one of the Xorg windows managers or sway.  
Open `lxappearance`and set either **Materia-dark-compact** or **ChromeOS-dark-compact** as the GTK theme.
Then set one of the **Papirus** icons themes depending on if you use a dark or light GTK theme or not and as your mouse cursor, select **Breeze Snow**.

### spicetify

If you use spotify, you can change Between **Dracula** and **Google** colors in `~/config/spicetify/.config/spicetify/config.ini` or alternatively `~/.config/spicetify/config.ini`. Then do

    spicetify apply backup && spicetify update

You'll have to do this after every `spotify` update.

## Shortcuts

### sway (Wayland)

look in the `sway` config for now

### sxhkd (Xorg)

`Alt` + `R` to reload bspwm and therefore also polybar. Useful when changing color schemes for example. Windows are preserved and you won't get kicked out of the session.

`Alt` + `Q` to quit bspwm

`Control` + `Super` + `S` to suspend the computer

`Control` + `Super` + `R` to reboot the computer

`Control` + `Super` + `P` to power off the computer

`Super` + `R` to start the rofi program launcher

`Super` + `C` to close a single window

`Super` + `Q` to kill a whole program

`Super` + `X` to start the Alacritty terminal

`Super` + `W` to start the Librewolf web browser

**!** Also take a look at the many other shortcuts in `~/.config/sxhkd/sxhkdrc` or `~/config/sxhkd/.config/sxhkd/sxhkdrc`

## Credits

- Credits to [raviolimavioli](https://www.instagram.com/raviolimavioli/) for the Arch Linux Chan background

## Licenses

### alacritty license:

Apache License
Version 2.0, January 2004
http://www.apache.org/licenses/

TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

1. Definitions.

  "License" shall mean the terms and conditions for use, reproduction,
  and distribution as defined by Sections 1 through 9 of this document.

  "Licensor" shall mean the copyright owner or entity authorized by
  the copyright owner that is granting the License.

  "Legal Entity" shall mean the union of the acting entity and all
  other entities that control, are controlled by, or are under common
  control with that entity. For the purposes of this definition,
  "control" means (i) the power, direct or indirect, to cause the
  direction or management of such entity, whether by contract or
  otherwise, or (ii) ownership of fifty percent (50%) or more of the
  outstanding shares, or (iii) beneficial ownership of such entity.

  "You" (or "Your") shall mean an individual or Legal Entity
  exercising permissions granted by this License.

  "Source" form shall mean the preferred form for making modifications,
  including but not limited to software source code, documentation
  source, and configuration files.

  "Object" form shall mean any form resulting from mechanical
  transformation or translation of a Source form, including but
  not limited to compiled object code, generated documentation,
  and conversions to other media types.

  "Work" shall mean the work of authorship, whether in Source or
  Object form, made available under the License, as indicated by a
  copyright notice that is included in or attached to the work
  (an example is provided in the Appendix below).

  "Derivative Works" shall mean any work, whether in Source or Object
  form, that is based on (or derived from) the Work and for which the
  editorial revisions, annotations, elaborations, or other modifications
  represent, as a whole, an original work of authorship. For the purposes
  of this License, Derivative Works shall not include works that remain
  separable from, or merely link (or bind by name) to the interfaces of,
  the Work and Derivative Works thereof.

  "Contribution" shall mean any work of authorship, including
  the original version of the Work and any modifications or additions
  to that Work or Derivative Works thereof, that is intentionally
  submitted to Licensor for inclusion in the Work by the copyright owner
  or by an individual or Legal Entity authorized to submit on behalf of
  the copyright owner. For the purposes of this definition, "submitted"
  means any form of electronic, verbal, or written communication sent
  to the Licensor or its representatives, including but not limited to
  communication on electronic mailing lists, source code control systems,
  and issue tracking systems that are managed by, or on behalf of, the
  Licensor for the purpose of discussing and improving the Work, but
  excluding communication that is conspicuously marked or otherwise
  designated in writing by the copyright owner as "Not a Contribution."

  "Contributor" shall mean Licensor and any individual or Legal Entity
  on behalf of whom a Contribution has been received by Licensor and
  subsequently incorporated within the Work.

2. Grant of Copyright License. Subject to the terms and conditions of
this License, each Contributor hereby grants to You a perpetual,
worldwide, non-exclusive, no-charge, royalty-free, irrevocable
copyright license to reproduce, prepare Derivative Works of,
publicly display, publicly perform, sublicense, and distribute the
Work and such Derivative Works in Source or Object form.

3. Grant of Patent License. Subject to the terms and conditions of
this License, each Contributor hereby grants to You a perpetual,
worldwide, non-exclusive, no-charge, royalty-free, irrevocable
(except as stated in this section) patent license to make, have made,
use, offer to sell, sell, import, and otherwise transfer the Work,
where such license applies only to those patent claims licensable
by such Contributor that are necessarily infringed by their
Contribution(s) alone or by combination of their Contribution(s)
with the Work to which such Contribution(s) was submitted. If You
institute patent litigation against any entity (including a
cross-claim or counterclaim in a lawsuit) alleging that the Work
or a Contribution incorporated within the Work constitutes direct
or contributory patent infringement, then any patent licenses
granted to You under this License for that Work shall terminate
as of the date such litigation is filed.

4. Redistribution. You may reproduce and distribute copies of the
Work or Derivative Works thereof in any medium, with or without
modifications, and in Source or Object form, provided that You
meet the following conditions:

(a) You must give any other recipients of the Work or
Derivative Works a copy of this License; and

(b) You must cause any modified files to carry prominent notices
stating that You changed the files; and

(c) You must retain, in the Source form of any Derivative Works
that You distribute, all copyright, patent, trademark, and
attribution notices from the Source form of the Work,
excluding those notices that do not pertain to any part of
the Derivative Works; and

(d) If the Work includes a "NOTICE" text file as part of its
distribution, then any Derivative Works that You distribute must
include a readable copy of the attribution notices contained
within such NOTICE file, excluding those notices that do not
pertain to any part of the Derivative Works, in at least one
of the following places: within a NOTICE text file distributed
as part of the Derivative Works; within the Source form or
documentation, if provided along with the Derivative Works; or,
within a display generated by the Derivative Works, if and
wherever such third-party notices normally appear. The contents
of the NOTICE file are for informational purposes only and
do not modify the License. You may add Your own attribution
notices within Derivative Works that You distribute, alongside
or as an addendum to the NOTICE text from the Work, provided
that such additional attribution notices cannot be construed
as modifying the License.

You may add Your own copyright statement to Your modifications and
may provide additional or different license terms and conditions
for use, reproduction, or distribution of Your modifications, or
for any such Derivative Works as a whole, provided Your use,
reproduction, and distribution of the Work otherwise complies with
the conditions stated in this License.

5. Submission of Contributions. Unless You explicitly state otherwise,
any Contribution intentionally submitted for inclusion in the Work
by You to the Licensor shall be under the terms and conditions of
this License, without any additional terms or conditions.
Notwithstanding the above, nothing herein shall supersede or modify
the terms of any separate license agreement you may have executed
with Licensor regarding such Contributions.

6. Trademarks. This License does not grant permission to use the trade
names, trademarks, service marks, or product names of the Licensor,
except as required for reasonable and customary use in describing the
origin of the Work and reproducing the content of the NOTICE file.

7. Disclaimer of Warranty. Unless required by applicable law or
agreed to in writing, Licensor provides the Work (and each
Contributor provides its Contributions) on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied, including, without limitation, any warranties or conditions
of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
PARTICULAR PURPOSE. You are solely responsible for determining the
appropriateness of using or redistributing the Work and assume any
risks associated with Your exercise of permissions under this License.

8. Limitation of Liability. In no event and under no legal theory,
whether in tort (including negligence), contract, or otherwise,
unless required by applicable law (such as deliberate and grossly
negligent acts) or agreed to in writing, shall any Contributor be
liable to You for damages, including any direct, indirect, special,
incidental, or consequential damages of any character arising as a
result of this License or out of the use or inability to use the
Work (including but not limited to damages for loss of goodwill,
work stoppage, computer failure or malfunction, or any and all
other commercial damages or losses), even if such Contributor
has been advised of the possibility of such damages.

9. Accepting Warranty or Additional Liability. While redistributing
the Work or Derivative Works thereof, You may choose to offer,
and charge a fee for, acceptance of support, warranty, indemnity,
or other liability obligations and/or rights consistent with this
License. However, in accepting such obligations, You may act only
on Your own behalf and on Your sole responsibility, not on behalf
of any other Contributor, and only if You agree to indemnify,
defend, and hold each Contributor harmless for any liability
incurred by, or claims asserted against, such Contributor by reason
of your accepting any such warranty or additional liability.

END OF TERMS AND CONDITIONS

APPENDIX: How to apply the Apache License to your work.

To apply the Apache License to your work, attach the following
boilerplate notice, with the fields enclosed by brackets "[]"
replaced with your own identifying information. (Don't include
the brackets!)  The text should be enclosed in the appropriate
comment syntax for the file format. We also recommend that a
file or class name and description of purpose be included on the
same "printed page" as the copyright notice for easier
identification within third-party archives.

Copyright 2020 The Alacritty Project

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

### bspwm and sxhkd license:

Copyright (c) 2012, Bastien Dejean
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### spicetify community themes (which occur in modified form in the "Pywal" theme) license:

MIT License

Copyright (c) 2019 morpheusthewhite

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

### picom license:

picom - a compositor for X11

Based on xcompmgr, originally written by Keith Packard, with modifications
from several contributors (according to the xcompmgr man page): Matthew Allum,
Eric Anholt, Dan Doel, Thomas Luebking, Matthew Hawn, Ely Levy, Phil Blundell,
and Carl Worth. Menu transparency was implemented by Dana Jansens.

Numerous contributions to picom from Richard Grenville.

See the CONTRIBUTORS file for a complete list of contributors

This source code is provided under:

    SPDX-License-Identifier: MPL-2.0 AND MIT

And the preferred license for new source files in this project is:

	 SPDX-License-Identifier: MPL-2.0

You can find the text of the licenses in the LICENSES directory.

### Dracula Theme license:

The MIT License (MIT)

Copyright (c) 2016 Dracula Theme

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

### Rate Arch Mirrors license

> inluded a snippet of documentation in `aliases/.aliases`

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

### dwm license

MIT/X Consortium License

© 2006-2019 Anselm R Garbe [anselm@garbe.ca](mailto:anselm@garbe.ca)  
© 2006-2009 Jukka Salmi [jukka at salmi dot ch]()  
© 2006-2007 Sander van Dijk [a dot h dot vandijk at gmail dot com]()  
© 2007-2011 Peter Hartlich [sgkkr at hartlich dot com]()  
© 2007-2009 Szabolcs Nagy [nszabolcs at gmail dot com]()  
© 2007-2009 Christof Musik [christof at sendfax dot de]()  
© 2007-2009 Premysl Hruby [dfenze at gmail dot com]()  
© 2007-2008 Enno Gottox Boland [gottox at s01 dot de]()  
© 2008 Martin Hurton [martin dot hurton at gmail dot com]()  
© 2008 Neale Pickett [neale dot woozle dot org]()  
© 2009 Mate Nagy [mnagy at port70 dot net]()   
© 2010-2016 Hiltjo Posthuma [hiltjo@codemadness.org](mailto:hiltjo@codemadness.org)  
© 2010-2012 Connor Lane Smith [cls@lubutu.com](mailto:cls@lubutu.com)  
© 2011 Christoph Lohmann [20h@r-36.net](mailto:20h@r-36.net)  
© 2015-2016 Quentin Rameau [quinq@fifth.space](mailto:quinq@fifth.space)  
© 2015-2016 Eric Pruitt eric.pruitt@gmail.com  
© 2016-2017 Markus Teich [markus.teich@stusta.mhn.de](mailto:markus.teich@stusta.mhn.de)  

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

### sway license

Copyright (c) 2016-2017 Drew DeVault

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
